<!-- #Alterar banner -->
<!-- Aqui você deve alterar os títulos e mensagens do banner e a imagem do background, e o link dos cliques -->
	<?php if (!$isMobile) : ?>
		<div class="slick-banner">
			<div style="--background: linear-gradient(rgba(36, 36, 36, 0.7) 50%, rgba(36, 36, 36, 0.7) 50%), url('<?= $url ?>imagens/banner/banner-1.webp')">
				<div>
					<div class="content-banner justify-content-evenly row">
						<div>
							<h1>Movendo o Transporte com Qualidade e Confiança</h1>
							<p>Especialistas em Peças para Carrocerias de Ônibus e Micro-Ônibus, com Atendimento de Excelência.</p>
							<a class="btn" href="<?= $url ?>catalogo" title="Página de produtos">Clique</a>
						</div>
						<div>
							<img src="<?= $url ?>imagens/clientes/logo-categoria.png" alt="" title="" class="slick-thumb">
						</div>
					</div>
				</div>
			</div>
			<div style="--background: linear-gradient(rgba(36, 36, 36, 0.5) 50%, rgba(36, 36, 36, 0.5) 50%), url('<?= $url ?>imagens/banner/banner-2.webp')">
				<div class="content-banner">
					<h2>Sua Conexão com Peças de Qualidade para Ônibus</h2>
					<p>Há 26 Anos Fornecendo Soluções em Peças para Carrocerias.</p>
					<a class="btn" href="<?= $url ?>sobre-nos" title="Página sobre nós">Clique</a>
				</div>
			</div>
	
		</div>
	<?php endif; ?>