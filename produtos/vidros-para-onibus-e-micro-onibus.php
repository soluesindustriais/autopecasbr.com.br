<?php
$h1 = "Vidros para Ônibus e Micro-Ônibus";
$title  =  $h1;
$cliente_minisite = "Federal Bus";
$minisite = "federalbus";
$desc = "Os Vidros para Ônibus e Micro-Ônibus oferecem resistência, segurança e visibilidade, ideais para transporte coletivo. Solicite uma cotação agora mesmo.";
include "inc/$minisite/inc/head.php";
?>
</head>

<body>
    <?php include "$prefix_includes" . "inc/formulario-personalizado.php" ?>
    <?php include "$prefix_includes" . "inc/topo.php"; ?>
    <?php include "$prefix_includes" . "inc/auto-breadcrumb.php" ?>

    <main class="mpi-page wrapper">
        <section class="product-container">
            <section class="product-information">
                <?php include "$prefix_includes" . "inc/product-images.php" ?>
                <?php include "$prefix_includes" . "inc/product-conteudo.php" ?>
            </section>
            <?php include "$prefix_includes" . "inc/product-aside.php" ?>
        </section>

        <?php include "$prefix_includes" . "inc/product-populares.php" ?>

    </main>
    <?php include "$prefix_includes" . "inc/footer.php"; ?>
</body>

</html>