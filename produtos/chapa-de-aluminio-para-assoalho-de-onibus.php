<?php
$h1 = "Chapa de Alumínio para Assoalho de Ônibus";
$title  =  $h1;
$cliente_minisite = "Federal Bus";
$minisite = "federalbus";
$desc = "A Chapa de Alumínio para Assoalho de Ônibus oferece resistência, leveza e durabilidade. Ideal para pisos de veículos. Solicite uma cotação agora mesmo.";
include "inc/$minisite/inc/head.php";
?>
</head>

<body>
    <?php include "$prefix_includes" . "inc/formulario-personalizado.php" ?>
    <?php include "$prefix_includes" . "inc/topo.php"; ?>
    <?php include "$prefix_includes" . "inc/auto-breadcrumb.php" ?>

    <main class="mpi-page wrapper">
        <section class="product-container">
            <section class="product-information">
                <?php include "$prefix_includes" . "inc/product-images.php" ?>
                <?php include "$prefix_includes" . "inc/product-conteudo.php" ?>
            </section>
            <?php include "$prefix_includes" . "inc/product-aside.php" ?>
        </section>

        <?php include "$prefix_includes" . "inc/product-populares.php" ?>

    </main>
    <?php include "$prefix_includes" . "inc/footer.php"; ?>
</body>

</html>