<header id="topnav" class="defaultscroll sticky">
   <div class="container">
      <!-- Logo container-->
      <div>
         <a title="logo" class="logo" href="<?=$url?>"><img class="mw-50 logo-img" src="imagens/logo/logo.png" alt="Logo"></a>
      </div>
      <div class="buy-button">
         <a title="logo" rel="noopener noreferrer"  href="https://www.solucoesindustriais.com.br/faca-parte" target="_blank" class="btn btn-primary">Gostaria de
            anunciar?</a>
      </div>
      <!--end login button-->
      <!-- End Logo container-->
      <div class="menu-extras">
         <div class="menu-item">
            <!-- Mobile menu toggle-->
            <a class="navbar-toggle">
               <div class="lines">
                  <span></span>
                  <span></span>
                  <span></span>
               </div>
            </a>
            <!-- End mobile menu toggle-->
         </div>
      </div>
      <div id="navigation">
         <!--end navigation menu-->
         <!-- Navigation Menu-->
         <ul class="navigation-menu">
            <li><a href="<?=$url?>">Início</a></li>

            <?php if(isset($categorias->getCategorias()[1])){ ?>
               <li class="has-submenu">
                  <a class="d-inline-block" href="<?=$url?>produtos">Produtos</a>
                  <span class="menu-arrow"></span>
                  <ul class="submenu">

                  <?
                  foreach($categorias->getCategorias() as $categoria):
                     $categoriaSemAcento = $trata->trataAcentos($categoria);
                     $categoriaSemHifen = $trata->retiraHifen($categoria);
                  ?>
                     <li class="has-submenu last-elements arrow-element">
                        <a class="d-inline-block" href="<?=$categoriaSemAcento."-categoria"; ?>"><?= $categoriaSemHifen; ?></a>
                        <span class="submenu-arrow"></span>
                        <ul class="submenu">
                           <? include("inc/".$categoriaSemAcento."/".$categoriaSemAcento."-sub-menu.php"); ?>
                        </ul>
                     </li>
                  <? endforeach; ?>
                  </ul>
               </li>
            <?php }else{ 
               $categoriaSemAcento = $trata->trataAcentos($categorias->getCategorias()[0]);
               $categoriaSemHifen = $trata->retiraHifen($categorias->getCategorias()[0]);
            ?>
               <li class="has-submenu">
               <a class="d-inline-block" href="<?=$categoriaSemAcento."-categoria"; ?>"><?= $categoriaSemHifen; ?></a>
                  <span class="menu-arrow"></span>
                  <ul class="submenu submenu-single">
                           <? include("inc/".$categoriaSemAcento."/".$categoriaSemAcento."-sub-menu.php"); ?>
                        </ul>

            <?php } ?>

            <li><a href="sobre-nos">Sobre nós</a></li>
            <li><a href="blog">Blog</a></li>
            <li><a href="https://faca-parte.solucoesindustriais.com.br/" target="_blank">Faça parte</a></li>
            <!--             <li class="has-submenu last-elements">
               <a href="javascript:void(0)">Informações</a><span class="menu-arrow"></span>
               <ul class="submenu megamenu">
                  <li class="active">
                     <ul>
                        <li class="active"><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI </a></li>
                        <li><a href="mpi">MPI</a></li>
                     </ul>
                  </li>
                  <li>
                     <ul>
                        <li class="active"><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI </a></li>
                        <li><a href="mpi">MPI</a></li>
                     </ul>
                  </li>
                  <li>
                     <ul>
                        <li class="active"><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI</a></li>
                        <li><a href="mpi">MPI </a></li>
                        <li><a href="mpi">MPI</a></li>
                     </ul>
                  </li>
               </ul>
            </li> -->
         </ul>
         <!--end navigation menu-->
         <!--end login button-->
      </div>
      <!--end navigation-->
   </div>
   <!--end container-->
</header>
<!--end header-->