<?php
$s = urldecode($URL[1]);
$h1 = 'Autor - '.$s;
$title = 'Autor - '.$s;
$desc = 'Autor - '.$s;
$key = '';
$var = 'Pesquisa de autor';

$Read->ExeRead(TB_USERS, "WHERE user_status = :st", "st=0");
$authors = $Read->getResult();
$authorKey = array_search($s, array_column($authors, 'user_name'));   
$itemAuthor = $authors[$authorKey]['user_id'];
$authorName = $authors[$authorKey]['user_name'];
$authorLastName = $authors[$authorKey]['user_lastname'];
$authorRole = $authors[$authorKey]['user_cargo'];
$authorCover = $authors[$authorKey]['user_cover'];
$authorAbout = $authors[$authorKey]['user_about'];
$authorTheme = $authors[$authorKey]['user_theme'];
$asideCategory = 'categorias';
$blogMonthList = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");

$Read->ExeRead(TB_CATEGORIA, "WHERE cat_status = :st", "st=2");
$categoryList = $Read->getResult();

include('inc/head.php');
include('inc/paginacao-blog-inc.php');

?>
<style>
  <?php
    switch($authorTheme){
      case "Grid": include('css/blog-grid.css');
        break;
      case "List": include('css/blog-list.css');
          break;
      case "Full": include('css/blog-full.css');
          break;
      default:
        break;
    }
  ?>
</style>
</head>
<body>
  <?php include('inc/topo-blog.php'); ?>
  <main>
    <?php
      switch($authorTheme){
        case "Grid": include('doutor/layout/autor/autor-grid.php');
          break;
        case "List": include('doutor/layout/autor/autor-list.php');
            break;
        case "Full": include('doutor/layout/autor/autor-full.php');
            break;
        default:
            break;
      }
      ?>
  </main>
  <?php include('inc/footer-blog.php'); ?>
</body>
</html>