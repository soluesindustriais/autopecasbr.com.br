<?php
$h1 = "Distribuidor de Para-Choques para Ônibus";
$title  =  $h1;
$cliente_minisite = "Federal Bus";
$minisite = "federalbus";
$desc = "O Distribuidor de Para-Choques para Ônibus fornece peças de alta qualidade e durabilidade, garantindo segurança e resistência. Solicite uma cotação.";
include "inc/$minisite/inc/head.php";
?>
<style>
    .extended-breadcrumb {
        background-color: var(--color-secundary);
        height: 100px;
        width: 100%;
    }

    .mpi-page {
        margin-top: -75px;
    }
</style>
</head>

<body>

    <?php include "$prefix_includes" . "inc/formulario-personalizado.php" ?>
    <?php include "$prefix_includes" . "inc/topo.php"; ?>
    <?php include "$prefix_includes" . "inc/auto-breadcrumb.php" ?>
    <div class="extended-breadcrumb">

    </div>
    <main class="mpi-page wrapper">
        <section class="product-container">
            <section class="product-information">
                <h2>SAIBA MAIS SOBRE <?php echo $h1 ?></h2>
                <?php include "$prefix_includes" . "inc/product-conteudo.php" ?>
                <?php include "$prefix_includes" . "inc/imagens-mpi.php" ?>

            </section>
            <?php include "$prefix_includes" . "inc/aside-mpi.php" ?>
        </section>

        <?php include "$prefix_includes" . "inc/product-populares.php" ?>

    </main>
    <?php include "$prefix_includes" . "inc/footer.php"; ?>
</body>

</html>