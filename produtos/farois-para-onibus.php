<?php
$h1 = "Faróis para Ônibus";
$title  =  $h1;
$cliente_minisite = "Federal Bus";
$minisite = "federalbus";
$desc = "Os Faróis para Ônibus oferecem iluminação eficiente e durável, garantindo segurança nas estradas. Conheça nossas opções e solicite uma cotação.";
include "inc/$minisite/inc/head.php";
?>
</head>

<body>
    <?php include "$prefix_includes" . "inc/formulario-personalizado.php" ?>
    <?php include "$prefix_includes" . "inc/topo.php"; ?>
    <?php include "$prefix_includes" . "inc/auto-breadcrumb.php" ?>

    <main class="mpi-page wrapper">
        <section class="product-container">
            <section class="product-information">
                <?php include "$prefix_includes" . "inc/product-images.php" ?>
                <?php include "$prefix_includes" . "inc/product-conteudo.php" ?>
            </section>
            <?php include "$prefix_includes" . "inc/product-aside.php" ?>
        </section>

        <?php include "$prefix_includes" . "inc/product-populares.php" ?>

    </main>
    <?php include "$prefix_includes" . "inc/footer.php"; ?>
</body>

</html>