
<section class="wrapper card-informativo">
    <h2>Nossos principais focos</h2>
        <div class="cards-informativos">
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-map"></i>
            <h3>Missão</h3>
            <p>Oferecer soluções completas em peças para carrocerias de ônibus e micro-ônibus, garantindo qualidade, eficiência e preço justo. Nosso objetivo é contribuir para o transporte seguro e confortável, proporcionando excelência no atendimento e agilidade nas entregas.</p>
        </div>
        <div class="cards-informativos-infos">
            <i class="fa-solid fa-eye"></i>
            <h3>Visão</h3>
        <p>Ser referência nacional no mercado de peças para carrocerias de ônibus e micro-ônibus, reconhecida pela confiabilidade, inovação e comprometimento com os nossos clientes e parceiros. Almejamos expandir nossa atuação, mantendo a excelência em produtos e serviços.</p>
    </div>
    <div class="cards-informativos-infos">
        <i class="fa-solid fa-handshake"></i>
        <h3>Valores</h3>
        <p>Nossos valores estão fundamentados na excelência no atendimento, qualidade e confiabilidade dos produtos, compromisso com o cliente, inovação constante, ética e transparência em todas as operações e na promoção de práticas sustentáveis.</p>
    </div>
</div>
</section>
