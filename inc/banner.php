        <div class="cd-hero">
    <ul class="cd-hero-slider autoplay">
        <li class="selected">
            <div class="cd-full-width">
                <h2>Peças para ônibus</h2>
                <p>As peças para ônibus são, normalmente, feitas de materiais que podem envolver vidro laminado ou temperado, plásticos, resinas, chapas de aço, dentre vários outros, que irão variar de acordo com o acessório ou peça que o cliente deseja adquirir.</p>
                <a href="<?=$url?>pecas-para-onibus" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>Chapas de Alumínio</h2>
                <p>Sempre que se fala de chapas de alumínio, pode ser descrito como uma peça produzida de liga de alumínio 1200-H14, que pode apresentar características lisas, xadrez ou corrugadas, bem como diversas medidas e espessuras.</p>
                <a href="<?=$url?>chapas-de-aluminio" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>Chapa Naval</h2>
                <p>A chapa naval nada mais é do que uma série de finas chapas que são sobrepostas uma sobre as outras, onde é aplicado uma pressão para que as mesmas sejam coladas. Entre as chapas, são utilizadas colas específicas para que as mesmas se fixem uma nas outras.</p>
                <a href="<?=$url?>chapa-naval" class="cd-btn">Saiba mais</a>
            </div>

        </li>
    </ul>
    <div class="cd-slider-nav">
        <nav>
            <span class="cd-marker item-1"></span>
            <ul>
                <li class="selected"><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
            </ul>
        </nav>
    </div>

</div>



        <!-- Hero End -->