 <section>
	<div style="background-color: var(--light);">
		<div class="container">
			<div class="wrapper">
				<div class="clientes">
					<h2 class="title-underline fs-28 text-center">Nossos Clientes</h2>
					<div class="clientes__carousel">
						<?php $imagens = glob("imagens/clientes/*", GLOB_BRACE);
						foreach ($imagens as $key => $imagem) : ?>
							<div class="clientes__item">
								<img class="clientes__image" src="<?= $url . $imagem ?>" alt="Cliente de <?= $nomeSite ?>" title="Cliente de <?= $nomeSite ?>" loading="lazy">
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>