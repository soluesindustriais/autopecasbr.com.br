<?
function criarLink($titulo) {
    // Convertendo para minúsculas
    $link = mb_strtolower($titulo, 'UTF-8');

    // Substituindo espaços por traços
    $link = str_replace(' ', '-', $link);

    // Removendo acentos e cedilha
    $link = iconv('UTF-8', 'ASCII//TRANSLIT', $link);

    // Removendo caracteres especiais restantes
    $link = preg_replace('/[^a-zA-Z0-9\-]/', '', $link);

    return $link;
}

// Exemplo de uso
$titulo1 = 'Duda Godoi';
$titulo2 = 'Kamila Fonseca';
$titulo3 = 'Outro Título Aqui';
$titulo4 = 'Mais Um Título Exemplo';
$titulo5 = 'Título com Acentuação';
$titulo6 = 'Último Exemplo Título';

$link1 = criarLink($titulo1);
$link2 = criarLink($titulo2);
$link3 = criarLink($titulo3);
$link4 = criarLink($titulo4);
$link5 = criarLink($titulo5);
$link6 = criarLink($titulo6);

?>